
#include <stdint.h>

#include "main.h"
#include "sense_i2c.h"
#include "mgm_i2c.h"
#include "mt_printf.h"
#include "sdr_definitions.h"
#include "sensors_templates.h"

extern void set_benchtop_payload_power_level( uint8_t new_power_level );

/*
 * Sensor reading callbacks
 *
 * These callbacks are assigned to each sensor when the sensors are created.
 * They are defined at the end of this file
 *
 * Sensors are create in the create_board_specific_sensors() hook below.
 *
 *
 */
sensor_reading_status_t sensor_reading_temp_pim400(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds);
sensor_reading_status_t sensor_reading_curr_pim400(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds);
sensor_reading_status_t sensor_reading_volt_a_pim400(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds);
sensor_reading_status_t sensor_reading_volt_b_pim400(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds);
sensor_reading_status_t sensor_reading_temp_MCP9902_X0(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds);
sensor_reading_status_t sensor_reading_temp_MCP9902_X1(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds);



/*
 * Hook for creating board specific sensors
 *
 * This function is called during the OpenIPMC initialization and is dedicated
 * for creating the board-specific sensors.
 */
void create_board_specific_sensors(void)
{

	void sensor_fpga_temp_action(ipmi_sel_entry_t* ipmi_sel_entry) {
		if (ipmi_sel_entry->event_dir_type == UPPER_CRITICAL) set_benchtop_payload_power_level( 0 );
	}

	uint8_t threshold_list[6];

// MCP9902 X0 temperature sensor
	threshold_list[0] = 0;    // Lower Non Recoverable  NOT USED
	threshold_list[1] = 0;    // Lower Critical         NOT USED
	threshold_list[2] = 0;    // Lower Non Critical     NOT USED
	threshold_list[3] = 80;   // Upper Non Critical     70.°C
	threshold_list[4] = 90;   // Upper Critical         80.°C
	threshold_list[5] = 100;  // Upper Non Recoverable  100. C
	analog_sensor_1_init_t sensor_params_X0 = {
	  .sensor_type=TEMPERATURE,
	  .base_unit_type=DEGREES_C,
	  .m=1,
	  .b=0,
	  .b_exp=0,
	  .r_exp=0,
	  .threshold_mask_set=UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE,   /* > 0 means that this threshold is settable AND readable via ipmitool. */
	  .threshold_mask_read=UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE, /* If set == 0 (above), read > 0 means that this threshold is readable. */
	  .threshold_list=threshold_list,
	  .id_string="X0 FPGA TEMP",
	  .get_sensor_reading_func=&sensor_reading_temp_MCP9902_X0,
	  .sensor_action_req=&sensor_fpga_temp_action
	};
	create_generic_analog_sensor_1(&sensor_params_X0);




	// MCP9902 X1 temperature sensor
	threshold_list[0] = 0;    // Lower Non Recoverable  NOT USED
	threshold_list[1] = 0;    // Lower Critical         NOT USED
	threshold_list[2] = 0;    // Lower Non Critical     NOT USED
	threshold_list[3] = 80;   // Upper Non Critical     70.°C
	threshold_list[4] = 90;   // Upper Critical         80.°C
	threshold_list[5] = 100;  // Upper Non Recoverable  100. C
	analog_sensor_1_init_t sensor_params_X1 = {
	  .sensor_type=TEMPERATURE,
	  .base_unit_type=DEGREES_C,
	  .m=1,
	  .b=0,
	  .b_exp=0,
	  .r_exp=0,
	  .threshold_mask_set=UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE,   /* > 0 means that this threshold is settable AND readable via ipmitool. */
	  .threshold_mask_read=UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE, /* If set == 0 (above), read > 0 means that this threshold is readable. */
	  .threshold_list=threshold_list,
	  .id_string="X1 FPGA TEMP",
	  .get_sensor_reading_func=&sensor_reading_temp_MCP9902_X1,
	  .sensor_action_req=&sensor_fpga_temp_action
	};
	create_generic_analog_sensor_1(&sensor_params_X1);

	// PIM400 temperature sensor
	threshold_list[0] = 0;    // Lower Non Recoverable  NOT USED
	threshold_list[1] = 0;    // Lower Critical         NOT USED
	threshold_list[2] = 0;    // Lower Non Critical     NOT USED
	threshold_list[3] = 41;   // Upper Non Critical     30.36°C
	threshold_list[4] = 46;   // Upper Critical         40.16°C
	threshold_list[5] = 0;    // Upper Non Recoverable  NOT USED
	analog_sensor_1_init_t sensor_params_PIM400T = {
	  .sensor_type=TEMPERATURE,
	  .base_unit_type=DEGREES_C,
	  .m=196,
	  .b=-50,
	  .b_exp=2,
	  .r_exp=-2,
	  .threshold_mask_set=UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE,   /* > 0 means that this threshold is settable AND readable via ipmitool. */
	  .threshold_mask_read=UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE, /* If set == 0 (above), read > 0 means that this threshold is readable. */
	  .threshold_list=threshold_list,
	  .id_string="TEMP PIM400",
	  .get_sensor_reading_func=&sensor_reading_temp_pim400,
	  .sensor_action_req=NULL
	};
	create_generic_analog_sensor_1(&sensor_params_PIM400T);

    
    // PIM400 current sensor
	threshold_list[0] = 0;    // Lower Non Recoverable  NOT USED
	threshold_list[1] = 0;    // Lower Critical         NOT USED
	threshold_list[2] = 0;    // Lower Non Critical     NOT USED
	threshold_list[3] = 0;    // Upper Non Critical     NOT USED
	threshold_list[4] = 0;    // Upper Critical         NOT USED
	threshold_list[5] = 0;    // Upper Non Recoverable  NOT USED
	analog_sensor_1_init_t sensor_params_PIM400A = {
	  .sensor_type=CURRENT,
	  .base_unit_type=AMPERES,
	  .m=94,
	  .b=0,
	  .b_exp=0,
	  .r_exp=-3,
	  .threshold_mask_set=0,   /* > 0 means that this threshold is settable AND readable via ipmitool. */
	  .threshold_mask_read=0, /* If set == 0 (above), read > 0 means that this threshold is readable. */
	  .threshold_list=threshold_list,
	  .id_string="CURRENT PIM400",
	  .get_sensor_reading_func=&sensor_reading_curr_pim400,
	  .sensor_action_req=NULL
	};
	create_generic_analog_sensor_1(&sensor_params_PIM400A);

    
    // PIM400 48V input sensors (A and B)
	analog_sensor_1_init_t sensor_params_PIM400VA = {
	  .sensor_type=VOLTAGE,
	  .base_unit_type=VOLTS,
	  .m=325,
	  .b=0,
	  .b_exp=0,
	  .r_exp=-3,
	  .threshold_mask_set=0,   /* > 0 means that this threshold is settable AND readable via ipmitool. */
	  .threshold_mask_read=0, /* If set == 0 (above), read > 0 means that this threshold is readable. */
	  .threshold_list=threshold_list,
	  .id_string="-48V_A PIM400",
	  .get_sensor_reading_func=&sensor_reading_volt_a_pim400,
	  .sensor_action_req=NULL
	};
	create_generic_analog_sensor_1(&sensor_params_PIM400VA);
    
	analog_sensor_1_init_t sensor_params_PIM400VB = {
	  .sensor_type=VOLTAGE,
	  .base_unit_type=VOLTS,
	  .m=325,
	  .b=0,
	  .b_exp=0,
	  .r_exp=-3,
	  .threshold_mask_set=0,   /* > 0 means that this threshold is settable AND readable via ipmitool. */
	  .threshold_mask_read=0, /* If set == 0 (above), read > 0 means that this threshold is readable. */
	  .threshold_list=threshold_list,
	  .id_string="-48V_B PIM400",
	  .get_sensor_reading_func=&sensor_reading_volt_b_pim400,
	  .sensor_action_req=NULL
	};
	create_generic_analog_sensor_1(&sensor_params_PIM400VB);



}


/*
 * Sensor Reading functions
 */

sensor_reading_status_t sensor_reading_temp_MCP9902_X0(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds)
{
	uint8_t tx_dataH, tx_dataL,  rx_dataH_0, rx_dataL_0, rx_dataH_1, rx_dataL_1;
	int reading_0, reading_1;
	uint32_t err[8];

	// Read the sensor twice to avoid errors
	tx_dataH = 0x01; //MCP9902 - Ext temp high part
	tx_dataL = 0x10; //MCP9902 - Ext temp low part
	err[0] = sense_i2c_transmit( 0xf8, &tx_dataH, 1, 100 );
	err[1] = sense_i2c_receive( 0xf8, &rx_dataH_0, 1, 100 );
	err[2] = sense_i2c_transmit( 0xf8, &tx_dataL, 1, 100 );
	err[3] = sense_i2c_receive( 0xf8, &rx_dataL_0, 1, 100 );
	err[4] = sense_i2c_transmit( 0xf8, &tx_dataH, 1, 100 );
	err[5] = sense_i2c_receive( 0xf8, &rx_dataH_1, 1, 100 );
	err[6] = sense_i2c_transmit( 0xf8, &tx_dataL, 1, 100 );
	err[7] = sense_i2c_receive( 0xf8, &rx_dataL_1, 1, 100 );
	
	for( int i=0; i<8; ++i )
		if( err[i] !=  HAL_I2C_ERROR_NONE )
			return SENSOR_READING_UNAVAILABLE;

	// 3+8 bit data is not digested by sensor function, better use the 8 bit one
	// reading_0 = (rx_dataH<<3) + (rx_dataL>>5) ;
	reading_0 = rx_dataH_0;
	reading_1 = rx_dataH_1;
	
	if( reading_0 != reading_1)
		return SENSOR_READING_UNAVAILABLE;

	sensor_reading->raw_value = reading_0;
	sensor_reading->present_state = 0;

	// Fill the threshold flag field
	if(reading_0 > 80)
		sensor_reading->present_state |= sensor_thresholds->upper_non_critical_threshold;
	if(reading_0 > 90)
		sensor_reading->present_state |= sensor_thresholds->upper_critical_threshold;
	if(reading_0 > 100)
	{
		sensor_reading->present_state |= sensor_thresholds->upper_non_recoverable_threshold;
		mt_printf_tstamp("X1: UPPER_NON_RECOVERABLE\r\n");
	}

	return SENSOR_READING_OK;
}

sensor_reading_status_t sensor_reading_temp_MCP9902_X1(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds)
{
	uint8_t tx_dataH, tx_dataL,  rx_dataH_0, rx_dataL_0, rx_dataH_1, rx_dataL_1;
	int reading_0, reading_1;
	uint32_t err[8];

	// Read the sensor twice to avoid errors
	tx_dataH = 0x01; //MCP9902 - Ext temp high part
	tx_dataL = 0x10; //MCP9902 - Ext temp low part
	err[0] = sense_i2c_transmit( 0x98, &tx_dataH, 1, 100 );
	err[1] = sense_i2c_receive( 0x98, &rx_dataH_0, 1, 100 );
	err[2] = sense_i2c_transmit( 0x98, &tx_dataL, 1, 100 );
	err[3] = sense_i2c_receive( 0x98, &rx_dataL_0, 1, 100 );
	err[4] = sense_i2c_transmit( 0x98, &tx_dataH, 1, 100 );
	err[5] = sense_i2c_receive( 0x98, &rx_dataH_1, 1, 100 );
	err[6] = sense_i2c_transmit( 0x98, &tx_dataL, 1, 100 );
	err[7] = sense_i2c_receive( 0x98, &rx_dataL_1, 1, 100 );

	for( int i=0; i<8; ++i )
		if( err[i] !=  HAL_I2C_ERROR_NONE )
			return SENSOR_READING_UNAVAILABLE;

	// 3+8 bit data is not digested by sensor function, better use the 8 bit one
	// reading_0 = (rx_dataH<<3) + (rx_dataL>>5) ;
	reading_0 = rx_dataH_0;
	reading_1 = rx_dataH_1;
	
	if( reading_0 != reading_1)
		return SENSOR_READING_UNAVAILABLE;

	sensor_reading->raw_value = reading_0;
	sensor_reading->present_state = 0;

	// Fill the threshold flag field
	if(reading_0 > 80)
		sensor_reading->present_state |= sensor_thresholds->upper_non_critical_threshold;
	if(reading_0 > 90)
		sensor_reading->present_state |= sensor_thresholds->upper_critical_threshold;
	if(reading_0 > 100)
	{
		sensor_reading->present_state |= sensor_thresholds->upper_non_recoverable_threshold;
		mt_printf_tstamp("X1: UPPER_NON_RECOVERABLE\r\n");
	}

	return SENSOR_READING_OK;
}

sensor_reading_status_t sensor_reading_temp_pim400(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds)
{
	uint8_t tx_data, rx_data_0, rx_data_1;
	uint32_t err[4];
	
	tx_data = 0x28; //PIM400 temperature register
	err[0] = mgm_i2c_transmit( 0x5E, &tx_data, 1, 100 );
	err[1] = mgm_i2c_receive( 0x5E, &rx_data_0, 1, 100 );
	err[2] = mgm_i2c_transmit( 0x5E, &tx_data, 1, 100 );
	err[3] = mgm_i2c_receive( 0x5E, &rx_data_1, 1, 100 );
	
	for( int i=0; i<4; ++i )
		if( err[i] !=  HAL_I2C_ERROR_NONE )
			return SENSOR_READING_UNAVAILABLE;
	
	if( rx_data_0 != rx_data_1)
		return SENSOR_READING_UNAVAILABLE;
	
	sensor_reading->raw_value = rx_data_0;

	sensor_reading->present_state = 0;
	return SENSOR_READING_OK;
}


sensor_reading_status_t sensor_reading_curr_pim400(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds)
{
	uint8_t tx_data, rx_data_0, rx_data_1;
	uint32_t err[4];
	
	tx_data = 0x21; //PIM400 current register
	err[0] = mgm_i2c_transmit( 0x5E, &tx_data, 1, 100 );
	err[1] = mgm_i2c_receive( 0x5E, &rx_data_0, 1, 100 );
	err[2] = mgm_i2c_transmit( 0x5E, &tx_data, 1, 100 );
	err[3] = mgm_i2c_receive( 0x5E, &rx_data_1, 1, 100 );
	
	for( int i=0; i<4; ++i )
		if( err[i] !=  HAL_I2C_ERROR_NONE )
			return SENSOR_READING_UNAVAILABLE;
		
	if( rx_data_0 != rx_data_1)
		return SENSOR_READING_UNAVAILABLE;
	
	sensor_reading->raw_value = rx_data_0;

	sensor_reading->present_state = 0;
	return SENSOR_READING_OK;
}


sensor_reading_status_t sensor_reading_volt_a_pim400(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds)
{
	uint8_t tx_data, rx_data_0, rx_data_1;
	uint32_t err[4];
	
	tx_data = 0x22; //PIM400 -48V_A register
	err[0] = mgm_i2c_transmit( 0x5E, &tx_data, 1, 100 );
	err[1] = mgm_i2c_receive( 0x5E, &rx_data_0, 1, 100 );
	err[2] = mgm_i2c_transmit( 0x5E, &tx_data, 1, 100 );
	err[3] = mgm_i2c_receive( 0x5E, &rx_data_1, 1, 100 );
	
	for( int i=0; i<4; ++i )
		if( err[i] !=  HAL_I2C_ERROR_NONE )
			return SENSOR_READING_UNAVAILABLE;
		
	if( rx_data_0 != rx_data_1)
		return SENSOR_READING_UNAVAILABLE;
	
	sensor_reading->raw_value = rx_data_0;

	sensor_reading->present_state = 0;
	return SENSOR_READING_OK;
}


sensor_reading_status_t sensor_reading_volt_b_pim400(sensor_reading_t* sensor_reading, sensor_thres_values_t *sensor_thresholds)
{
	uint8_t tx_data, rx_data_0, rx_data_1;
	uint32_t err[4];
	
    tx_data = 0x23; //PIM400 -48V_B register
	err[0] = mgm_i2c_transmit( 0x5E, &tx_data, 1, 100 );
	err[1] = mgm_i2c_receive( 0x5E, &rx_data_0, 1, 100 );
	err[2] = mgm_i2c_transmit( 0x5E, &tx_data, 1, 100 );
	err[3] = mgm_i2c_receive( 0x5E, &rx_data_1, 1, 100 );
	
	for( int i=0; i<4; ++i )
		if( err[i] !=  HAL_I2C_ERROR_NONE )
			return SENSOR_READING_UNAVAILABLE;
		
	if( rx_data_0 != rx_data_1)
		return SENSOR_READING_UNAVAILABLE;
	
    sensor_reading->raw_value = rx_data_0;

	sensor_reading->present_state = 0; // No thresholds supported by this sensor

	return SENSOR_READING_OK;
}
