

.DEFAULT_GOAL := hpm


# Guarantee updated submodules
init:
	@echo Updating Submodules
	@git submodule update --init --recursive


# Generate the .bin and .hpm into openipmc-fw/CM7 and 
# openipmc-fw/CM4 directories.
# For convenience, these files are copied into the project root
hpm: init
	@cd openipmc-fw                  && \
	 $(MAKE) hpm                     && \
	 cp CM4/openipmc-fw_CM4.bin ../  && \
	 cp CM4/upgrade_CM4.hpm     ../  && \
	 cp CM7/openipmc-fw_CM7.bin ../  && \
	 cp CM7/upgrade_CM7.hpm     ../


clean:
	@cd openipmc-fw && $(MAKE) clean
	@rm -f openipmc-fw_CM4.bin
	@rm -f upgrade_CM4.hpm 
	@rm -f openipmc-fw_CM7.bin
	@rm -f upgrade_CM7.hpm

